DECLARE @BlackListRN AS TABLE (

RNBlackList varchar(30)
)

INSERT INTO @BlackListRN VALUES
(084106768)
,(031176110)
,(064000059)
,(064101233)
,(064102070)
,(071025661)
,(071025661)
,(072403473)
,(084308003)
,(111901519)
,(112206763)
,(113008465)
,(122000496)
,(265473508)
,(271070801)
,(284283261)
,(311981614)
,(311985788)
,(313083646)
,(313177785)
,(314972853)
,(314977188)
,(321171184)
,(322282001)
,(121000358)
,(253177049)
,(071000013)
,(081904808)
,(071923909)
,(253184537)
,(253279031)
,(114000093)


DECLARE @StartDate DATE
DECLARE @EndDate DATE

SET @StartDate = '06/01/2021'
SET @EndDate = CURRENT_TIMESTAMP

SELECT 
     LI.[LoanID]
    ,LI.[CustomerID]
	,LI.ApplicantId
    ,Clarity.SSN
    ,LI.[Office]
    ,LI.[FundingDate]
    ,LI.[FundedAmount]
    ,LIN.LoanIncreaseFundedAmount
	,LIN.TotalLoanIncreases
    ,LI.[Type] 
	,LI.[LoanTypeDescription] AS LoanTypeName
    ,LI.[LoanStatus]
    ,LI.[PaymentsAmount]
    ,LI.[InterestPaid]
    ,LI.[PrincipalPaid]
    ,LI.[WODate]
    ,LI.[WOAmount]
    ,LI.[IsCycled]
    ,LI.[IsFPD]
	,CASE WHEN ReturnCode IN ('R01','R09','R99') or  ReturnCode is null THEN 0 ELSE 1 END IsFPF
    ,LI.[IsZPD]    
    ,LI.[Source]
    ,LI.[Affiliate]
    ,LI.[Advertisement]
	,CASE WHEN  Clarity.ApplicantId  IS NULL   THEN 'Control Flow' 
		WHEN Clarity.ApplicantId  IS NOT NULL THEN 'Test Flow 1'  END AS FlowName 
    ,COALESCE(AM.MigrationType,'NL') AS MigrationType
	,CustomerInfo.Zip
	,CustomerInfo.[State]
	,CustomerInfo.Salary
	,CustomerInfo.PayFrequency
	,CustomerInfo.Advertisement
	,AgeOfCustomer
	,LeadStage.TrafficSource
	,LeadStage.LeadStatus
	,LeadStage.LeadDate
	,LeadStage.ClearCreditRiskScore
	,LeadStage.ClearBankBehaviorScoreV2
	,FTR.StoreID
	,FTR.RiskScore
	,AddressInfo.Name AS StateName
	,BankInfo.Name AS BankName
	,BankInfo.RoutingNum
	,BankInfo.MonthLenght AS BankAccountLenghtMonths 
	,BankInfo.YearsLenght AS BankAccountLenghtYears	
	,CASE WHEN BankInfo.RoutingNum IN (SELECT RNBlackList  FROM @BlackListRN) THEN 1 ELSE 0 END IsRoutingNumberBlackList
	,EmployerInfo.MonthsOnJob AS MonthsOnJob
	,EmployerInfo.YearsOnJob AS YearsOnJob 
	,JSON_VALUE(JSON_VALUE(PTIResponse,'$.Response'),'$.PaymentAmount') AS PaymentAmount
    ,JSON_VALUE(JSON_VALUE(PTIResponse,'$.Response'),'$.MonthlyIncome') AS MonthlyIncome
    ,JSON_VALUE(JSON_VALUE(PTIResponse,'$.Response'),'$.TWNSalary') AS UsedTWNSalary
    ,JSON_VALUE(JSON_VALUE(PTIResponse,'$.Response'),'$.PTIResult') AS PTIResult
    ,JSON_VALUE(JSON_VALUE(PTIResponse,'$.Response'),'$.PayFrequency') AS PayFrequencyPTI
	,ClarityAttributes.number_of_accounts_at_high_risk_banks
	,ClarityAttributes.number_of_accounts_with_alternate_ssns
	,ClarityAttributes.number_of_loans_past_due
	,ClarityAttributes.number_of_payday_inquiries_number_since_last_inquiry
	,ClarityAttributes.days_since_last_check_cashing_activity
	,ClarityAttributes.days_since_last_loan_paid_off
	,ClarityAttributes.days_since_last_loan_payment
	,ClarityAttributes.days_since_last_ontime_payment
	,ClarityAttributes.number_of_bank_accounts
	,ClarityTradelineAttributes.[BB_Acct1_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct1_validated_thru_trades]
	,ClarityTradelineAttributes.[BB_Acct1_dflt_rate_60_days]
	,ClarityTradelineAttributes.[BB_Acct1_dflt_rate_61_365]
	,ClarityTradelineAttributes.[BB_Acct1_dflt_rate_ratio]
	,ClarityTradelineAttributes.[BB_Acct1_dsnce_validated_trade]
	,ClarityTradelineAttributes.[BB_Acct1_dsnce_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct2_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct2_validated_thru_trades]
	,ClarityTradelineAttributes.[BB_Acct2_dflt_rate_60_days]
	,ClarityTradelineAttributes.[BB_Acct2_dflt_rate_61_365]
	,ClarityTradelineAttributes.[BB_Acct2_dflt_rate_ratio]
	,ClarityTradelineAttributes.[BB_Acct2_dsnce_validated_trade]
	,ClarityTradelineAttributes.[BB_Acct2_dsnce_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct3_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct3_validated_thru_trades]
	,ClarityTradelineAttributes.[BB_Acct3_dflt_rate_60_days]
	,ClarityTradelineAttributes.[BB_Acct3_dflt_rate_61_365]
	,ClarityTradelineAttributes.[BB_Acct3_dflt_rate_ratio]
	,ClarityTradelineAttributes.[BB_Acct3_dsnce_validated_trade]
	,ClarityTradelineAttributes.[BB_Acct3_dsnce_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct4_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct4_validated_thru_trades]
	,ClarityTradelineAttributes.[BB_Acct4_dflt_rate_60_days]
	,ClarityTradelineAttributes.[BB_Acct4_dflt_rate_61_365]
	,ClarityTradelineAttributes.[BB_Acct4_dflt_rate_ratio]
	,ClarityTradelineAttributes.[BB_Acct4_dsnce_validated_trade]
	,ClarityTradelineAttributes.[BB_Acct4_dsnce_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct5_dflt_hist]
	,ClarityTradelineAttributes.[BB_Acct5_validated_thru_trades]
	,ClarityTradelineAttributes.[BB_Acct5_dflt_rate_60_days]
	,ClarityTradelineAttributes.[BB_Acct5_dflt_rate_61_365]
	,ClarityTradelineAttributes.[BB_Acct5_dflt_rate_ratio]
	,ClarityTradelineAttributes.[BB_Acct5_dsnce_validated_trade]
	,ClarityTradelineAttributes.[BB_Acct5_dsnce_dflt_hist]
	,ClarityTradelineAttributes.[CCR_hit]
	,ClarityTradelineAttributes.[CCR_too_many_tradelines]
	,ClarityTradelineAttributes.[CCR_worst_pmt_rating]
	,ClarityTradelineAttributes.[CCR_count_one_cycle_past_due]
	,ClarityTradelineAttributes.[CCR_count_two_cycles_past_due]
	,ClarityTradelineAttributes.[CCR_count_three_cycles_past_due]
	,ClarityTradelineAttributes.[CCR_dsince_1st_bankacct_1st_seen]
	,ClarityTradelineAttributes.[CCR_dsince_1st_bankacct_prv_seen]
	,ClarityTradelineAttributes.[CCR_dsince_1st_loan_opened]
	,ClarityTradelineAttributes.[CCR_dsince_1st_loan_pd_off]
	,ClarityTradelineAttributes.[CCR_dsince_1st_ontime_pmt]
	,ClarityTradelineAttributes.[CCR_dsince_inq_1st_seen]
	,ClarityTradelineAttributes.[CCR_dsince_inq_prv_seen]
	,ClarityTradelineAttributes.[CCR_dsince_lst_loan_chrg_off]
	,ClarityTradelineAttributes.[CCR_dsince_lst_loan_in_colls]
	,ClarityTradelineAttributes.[CCR_dsince_lst_loan_opened]
	,ClarityTradelineAttributes.[CCR_dsince_lst_loan_pd_off]
	,ClarityTradelineAttributes.[CCR_dsince_lst_loan_pmt]
	,ClarityTradelineAttributes.[CCR_dsince_lst_ontime_pmt]
	,TWNAttributes.DateRequested AS TWNDateRequested
	,TWNAttributes.XmlIn AS TWNXmlIn
	,TWNAttributes.ServiceResponseDetail AS TWNServiceResponseDetail
	,TWNAttributes.SuccessfulResponse AS TWNSuccessfulResponse
FROM 
    [BusinessIntelligenceMFCX].[dbo].[fact_LoanInfo] AS LI 
	CROSS APPLY (
		SELECT TOP 1  Social FROM ApplicationProcess.dbo.Applicant WHERE Applicant.Applicantid = LI.ApplicantID
	) AS ApplicantInfo
	OUTER APPLY (
		SELECT TOP 1 
			 TrafficSource
			,LeadStatus
			,LeadDate
			,ClarityScore AS ClearCreditRiskScore
			,ClarityCBBScore2 AS ClearBankBehaviorScoreV2
		FROM 
			DATAWAREHOUSEMFCX.MFCXStage.LeadsStage
		WHERE LeadsStage.ApplicantID = LI.ApplicantID AND LeadStatus = 'accepted'
	) AS LeadStage
	LEFT JOIN ApplicationProcess.dbo.ApplicantMigrated AS AM
        ON LI.ApplicantId = AM.ApplicantId
    OUTER APPLY (
        SELECT 
             SUM(FundedAmount) AS LoanIncreaseFundedAmount
            ,COUNT(FundedAmount) AS TotalLoanIncreases
        FROM 
            BusinessIntelligenceMFCX.dbo.fact_LILoanInfo
        WHERE 
            fact_LILoanInfo.LoanID = LI.LoanID
    ) AS LIN	
	OUTER APPLY (
		SELECT   
			 APP.ApplicantAlternativeID AS ApplicantID
			,App.SSN
		FROM 
			DATAWAREHOUSEMFCX.MFCXLeads.ClarityDecisionResult AS CDR
		INNER JOIN DATAWAREHOUSEMFCX.MFCXLeads.Applicants as App
			ON CDR.ApplicantID = App.ApplicantId
		WHERE App.ApplicantAlternativeID = LI.ApplicantId
	) AS Clarity
	OUTER APPLY (
		SELECT 
			TOP 1 CustomerID
			,LoanID
			,FundingDate
			,Office
			,Zip
			,[State]
			,Salary
			,PaidFrequency AS PayFrequency
			,Advertizement AS Advertisement
			,AgeOfCustomer
		FROM 
			[BusinessIntelligenceMFCX].dbo.[dim_Customer]
			WHERE [dim_Customer].LoanID = LI.LoanID
	) AS CustomerInfo
   OUTER APPLY(
        SELECT 
         TOP 1    P1.ReturnCode
        FROM 
            BusinessIntelligenceMFCX.dbo.fact_PaymentInfo AS P1
        WHERE 
            P1.LoanId = LI.LoanId
            AND P1.PeriodNumber = 1
			AND P1.ReturnCode IS NOT NULL
   ) AS FPM
   OUTER APPLY (
	    SELECT TOP(1)
	        StoreID
	        ,RiskScore
	    FROM 
		    WebServiceManager.dbo.FactorTrustResponse
	    WHERE 
            EntityId = LI.ApplicantID
		    AND StoreID = '0001'
	    ORDER BY FactorTrustResponse.FactorTrustResponseID DESC
    ) AS FTR -- FTR: Factor Trust Risk Score
	OUTER APPLY (
	   SELECT TOP 1 
			BankInformation.RoutingNum
		   ,BankInformation.Name
		   ,BankInformation.MonthLenght
		   ,BankInformation.YearsLenght
	  FROM ApplicationProcess.dbo.BankInformationXCustomer
	  INNER JOIN ApplicationProcess.dbo.BankInformation
		 ON BankInformationXCustomer.BankInformationID = BankInformation.BankInformationID
	  WHERE BankInformationXCustomer.CustomerId  = LI.CustomerID
	) AS BankInfo
	OUTER APPLY (
	   SELECT TOP 1 
			Employer.MonthsOnJob
			,Employer.YearsOnJob
	  FROM ApplicationProcess.dbo.EmployerXCustomer
	  INNER JOIN ApplicationProcess.dbo.Employer
		 ON EmployerXCustomer.EmployerId = Employer.EmployerID
	  WHERE EmployerXCustomer.CustomerId  = LI.CustomerID
	) AS EmployerInfo
	OUTER APPLY (
		SELECT TOP 1 
			State.Name
		  FROM 
			ApplicationProcess.dbo.Address
		  INNER JOIN  ApplicationProcess.dbo.State
			 ON Address.StateID = State.StateID
		  INNER JOIN ApplicationProcess.dbo.AddressXCustomer
			ON AddressXCustomer.AddressID = Address.AddressID
		  WHERE AddressXCustomer.CustomerID =  LI.CustomerID
	) AS AddressInfo
OUTER APPLY (
	SELECT TOP 1 
		  [number_of_accounts_with_alternate_ssns]
		  ,[number_of_accounts_at_high_risk_banks]
		  ,[number_of_loans_past_due]
		  ,[number_of_payday_inquiries_number_since_last_inquiry]
		  ,[days_since_last_check_cashing_activity]
		  ,[days_since_last_loan_paid_off]
		  ,[days_since_last_loan_payment]
		  ,[days_since_last_ontime_payment]
		  ,[number_of_bank_accounts]
	FROM   
		[DATAWAREHOUSEMFCX].[MFCXStage].[vi_Clarityservicelog]
	WHERE CONVERT(DATE,[vi_Clarityservicelog].[DateRequested]) = CONVERT(DATE,LeadStage.LeadDate)
	AND [vi_Clarityservicelog].[SSN] = replace(ApplicantInfo.Social,'-','')
	) AS ClarityAttributes
OUTER APPLY (
	SELECT TOP 1 
		  [ServiceInquiryLogId]
		  ,[DateRequested]
		  ,[ServiceResponseDetail]
		  ,[SSN]
		  ,[BB_Acct1_dflt_hist]
		  ,[BB_Acct1_validated_thru_trades]
		  ,[BB_Acct1_dflt_rate_60_days]
		  ,[BB_Acct1_dflt_rate_61_365]
		  ,[BB_Acct1_dflt_rate_ratio]
		  ,[BB_Acct1_dsnce_validated_trade]
		  ,[BB_Acct1_dsnce_dflt_hist]
		  ,[BB_Acct2_dflt_hist]
		  ,[BB_Acct2_validated_thru_trades]
		  ,[BB_Acct2_dflt_rate_60_days]
		  ,[BB_Acct2_dflt_rate_61_365]
		  ,[BB_Acct2_dflt_rate_ratio]
		  ,[BB_Acct2_dsnce_validated_trade]
		  ,[BB_Acct2_dsnce_dflt_hist]
		  ,[BB_Acct3_dflt_hist]
		  ,[BB_Acct3_validated_thru_trades]
		  ,[BB_Acct3_dflt_rate_60_days]
		  ,[BB_Acct3_dflt_rate_61_365]
		  ,[BB_Acct3_dflt_rate_ratio]
		  ,[BB_Acct3_dsnce_validated_trade]
		  ,[BB_Acct3_dsnce_dflt_hist]
		  ,[BB_Acct4_dflt_hist]
		  ,[BB_Acct4_validated_thru_trades]
		  ,[BB_Acct4_dflt_rate_60_days]
		  ,[BB_Acct4_dflt_rate_61_365]
		  ,[BB_Acct4_dflt_rate_ratio]
		  ,[BB_Acct4_dsnce_validated_trade]
		  ,[BB_Acct4_dsnce_dflt_hist]
		  ,[BB_Acct5_dflt_hist]
		  ,[BB_Acct5_validated_thru_trades]
		  ,[BB_Acct5_dflt_rate_60_days]
		  ,[BB_Acct5_dflt_rate_61_365]
		  ,[BB_Acct5_dflt_rate_ratio]
		  ,[BB_Acct5_dsnce_validated_trade]
		  ,[BB_Acct5_dsnce_dflt_hist]
		  ,[CCR_hit]
		  ,[CCR_too_many_tradelines]
		  ,[CCR_worst_pmt_rating]
		  ,[CCR_count_one_cycle_past_due]
		  ,[CCR_count_two_cycles_past_due]
		  ,[CCR_count_three_cycles_past_due]
		  ,[CCR_dsince_1st_bankacct_1st_seen]
		  ,[CCR_dsince_1st_bankacct_prv_seen]
		  ,[CCR_dsince_1st_loan_opened]
		  ,[CCR_dsince_1st_loan_pd_off]
		  ,[CCR_dsince_1st_ontime_pmt]
		  ,[CCR_dsince_inq_1st_seen]
		  ,[CCR_dsince_inq_prv_seen]
		  ,[CCR_dsince_lst_loan_chrg_off]
		  ,[CCR_dsince_lst_loan_in_colls]
		  ,[CCR_dsince_lst_loan_opened]
		  ,[CCR_dsince_lst_loan_pd_off]
		  ,[CCR_dsince_lst_loan_pmt]
		  ,[CCR_dsince_lst_ontime_pmt]
	FROM   
		DATAWAREHOUSEMFCX.MFCXStage.vi_ClarityTradelineAttributes
	WHERE CONVERT(DATE,vi_ClarityTradelineAttributes.DateRequested) = CONVERT(DATE,LeadStage.LeadDate)
	AND vi_ClarityTradelineAttributes.SSN = replace(ApplicantInfo.Social,'-','')
) AS ClarityTradelineAttributes
OUTER APPLY (
	SELECT 
		App.ApplicantId
		,SIL.DateRequested
		,SIL.XmlIn
		,SIL.ServiceResponseDetail
		,SIL.SuccessfulResponse
	FROM ApplicationProcess.dbo.ServiceInquiryLog AS SIL
	--FROM [MFCXStage].ServiceInquiryLog AS SIL
	INNER JOIN ApplicationProcess.dbo.Applicant AS App
	--INNER JOIN [MFCXStage].Applicant AS App
	ON App.ApplicantId = SUBSTRING(XMLIn,CHARINDEX('EntityId',XMLIn) + 10, (CHARINDEX('"LoggedUserId"',XMLIn) -1) - (CHARINDEX('EntityId',XMLIn) + 10) )
	WHERE SIL.ServiceId = 21 AND App.ApplicantID = LI.ApplicantId
	--ORDER BY applicantID DESC
) AS TWNAttributes
  LEFT JOIN [DATAWAREHOUSEMFCX].[MFCXStage].[MethodServiceInputOutput] AS PTI
      ON LI.ApplicantId = PTI.EntityId AND LI.[Type] = 'New Loan'
WHERE
	LI.LoanStatus <> 'cancelled'
	AND LI.[FundingDate] BETWEEN @StartDate AND @EndDate
	--AND LI.LoanID = 47235
 