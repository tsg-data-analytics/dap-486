# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 18:04:22 2022

@author: manuelc
"""


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import math 

import itertools
import datetime
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold

from collections import OrderedDict

import json
from datetime import datetime		

#########################################################################################
#                                                                                       #
#                                     FUNCTIONS                                         #
#                                                                                       #
#########################################################################################

def create_corr(ds, method='kendall', title='Correlation Matrix', figsize = (20,20)):
    corr_matrix_ds = ds.corr(method=method).round(2)
    mask = np.triu(np.ones_like(corr_matrix_ds, dtype=bool))
    fig, ax = plt.subplots(figsize=figsize) 
    g= sns.heatmap(corr_matrix_ds, annot=True, vmax=1, vmin=-1, center=0, cmap='vlag', mask=mask)
    g.set_xticklabels(g.get_xticklabels(), rotation = 45, ha='right')
    plt.title(title)
    plt.show()
    
def score_distribution(dataset, var, bin_num, target, ylim = 1, title = 'Score Distribution'):
    var_discretised, intervals = pd.qcut(dataset[var], bin_num, labels=None, retbins=True, precision=3, duplicates='drop')
    # NewScore_discretised = pd.cut(dataset.NewFT_Score, 10)
    dataset[var +'_cat'] = var_discretised # categorical variable
        
    a = dataset.groupby([var +'_cat'])[target].count()
    print(a)
    b = dataset.groupby([var +'_cat'])[target].sum()
    # print "{0:.0%}".format(1./3)
    temp = pd.concat([a,b], axis = 1)
    temp.columns = ['Loan Volume', target]
    temp.plot(kind = 'bar')
    plt.ylabel('Volume')
    plt.legend(loc = 2)
    
    c = dataset.groupby([var +'_cat'])[target].mean() #np.float(len(data))
    x = dataset[var +'_cat'].unique().astype(str)
    y = round(c, 2)
    axes2 = plt.twinx()
    c.plot(kind = 'line', color='red')
    axes2.set_ylim(0, ylim)
    axes2.set_ylabel(target+'Rate')
    for x, y in zip(range(len(x)), y):
        plt.text(x, y + 0.02, y, ha = 'center', va = 'bottom')
        plt.xticks(rotation= 90)
        
    plt.title(title)
    plt.show()


#########################################################################################
#                                                                                       #
#                                   LOAD DATASETS                                       #
#                                                                                       #
#########################################################################################

filename = r'C:\Users\manuelc\OneDrive - The Strategic Group\Documents\Sprints\Sprint_19\DAP-486\MFC_Loans_Tradeline_Attributes.csv'
ds_loans = pd.read_csv(filename)

#########################################################################################
#                                                                                       #
#                                   DATA CLEANING                                       #
#                                                                                       #
#########################################################################################

start_date = '8/24/2021'
end_date = '1/31/2022'

ds_loans = ds_loans[ds_loans['FlowName']=='Test Flow 1']

ds_loans['FundingDate'] = pd.to_datetime(ds_loans['FundingDate'], errors='coerce')

ds_loans = ds_loans[(ds_loans['FundingDate'] >= start_date)
                      & (ds_loans['FundingDate'] <= end_date)
                      ]

ds_loans.rename(columns={
                      'ApplicantId': 'ApplicantID',
                      'Total Leads':'TotalLeads'
                      ,'Payquency': 'PayFrequency'
                      ,'Age': 'AgeOfCustomer'
                      ,'LoanAmount': 'FundedAmount'
                }, inplace=True)

ds_loans['PTIResult'] = ds_loans['PTIResult'].str.strip('%').astype('float')
ds_loans['MonthlyIncome'] = ds_loans['MonthlyIncome'].str.replace(',', '').str.strip('$').astype('float')
ds_loans['TotalMonthsOnJob'] = (ds_loans['YearsOnJob']*12) + ds_loans['MonthsOnJob']
ds_loans['TotalMonthsAccLen'] = (ds_loans['BankAccountLenghtYears']*12) + ds_loans['BankAccountLenghtMonths']
ds_loans['RoutingNum'] = ds_loans['RoutingNum'].astype(str) 
ds_loans['RoutingNum'] = ds_loans['RoutingNum'].str.pad(9, side='left', fillchar='0')

#########################################################################################
#                                                                                       #
#                            CLARITY CORRELATION VALUES                                 #
#                                                                                       #
#########################################################################################

tradeline_cols = [
                    'number_of_accounts_at_high_risk_banks'
                    ,'number_of_accounts_with_alternate_ssns'
                    ,'number_of_loans_past_due'
                    ,'number_of_payday_inquiries_number_since_last_inquiry'
                    ,'days_since_last_check_cashing_activity'
                    ,'days_since_last_loan_paid_off'
                    ,'days_since_last_loan_payment'
                    ,'days_since_last_ontime_payment'
                    ,'number_of_bank_accounts'
                    ,'BB_Acct1_dflt_hist'
                    ,'BB_Acct1_validated_thru_trades'
                    ,'BB_Acct1_dflt_rate_60_days'
                    ,'BB_Acct1_dflt_rate_61_365'
                    ,'BB_Acct1_dflt_rate_ratio'
                    ,'BB_Acct1_dsnce_validated_trade'
                    ,'BB_Acct1_dsnce_dflt_hist'
                    ,'BB_Acct2_dflt_hist'
                    ,'BB_Acct2_validated_thru_trades'
                    ,'BB_Acct2_dflt_rate_60_days'
                    ,'BB_Acct2_dflt_rate_61_365'
                    ,'BB_Acct2_dflt_rate_ratio'
                    ,'BB_Acct2_dsnce_validated_trade'
                    ,'BB_Acct2_dsnce_dflt_hist'
                    ,'BB_Acct3_dflt_hist'
                    ,'BB_Acct3_validated_thru_trades'
                    ,'BB_Acct3_dflt_rate_60_days'
                    ,'BB_Acct3_dflt_rate_61_365'
                    ,'BB_Acct3_dflt_rate_ratio'
                    ,'BB_Acct3_dsnce_validated_trade'
                    ,'BB_Acct3_dsnce_dflt_hist'
                    ,'BB_Acct4_dflt_hist'
                    ,'BB_Acct4_validated_thru_trades'
                    ,'BB_Acct4_dflt_rate_60_days'
                    ,'BB_Acct4_dflt_rate_61_365'
                    ,'BB_Acct4_dflt_rate_ratio'
                    ,'BB_Acct4_dsnce_validated_trade'
                    ,'BB_Acct4_dsnce_dflt_hist'
                    ,'BB_Acct5_dflt_hist'
                    ,'BB_Acct5_validated_thru_trades'
                    ,'BB_Acct5_dflt_rate_60_days'
                    ,'BB_Acct5_dflt_rate_61_365'
                    ,'BB_Acct5_dflt_rate_ratio'
                    ,'BB_Acct5_dsnce_validated_trade'
                    ,'BB_Acct5_dsnce_dflt_hist'
                    ,'CCR_hit'
                    ,'CCR_too_many_tradelines'
                    ,'CCR_worst_pmt_rating'
                    ,'CCR_count_one_cycle_past_due'
                    ,'CCR_count_two_cycles_past_due'
                    ,'CCR_count_three_cycles_past_due'
                    ,'CCR_dsince_1st_bankacct_1st_seen'
                    ,'CCR_dsince_1st_bankacct_prv_seen'
                    ,'CCR_dsince_1st_loan_opened'
                    ,'CCR_dsince_1st_loan_pd_off'
                    ,'CCR_dsince_1st_ontime_pmt'
                    ,'CCR_dsince_inq_1st_seen'
                    ,'CCR_dsince_inq_prv_seen'
                    ,'CCR_dsince_lst_loan_chrg_off'
                    ,'CCR_dsince_lst_loan_in_colls'
                    ,'CCR_dsince_lst_loan_opened'
                    ,'CCR_dsince_lst_loan_pd_off'
                    ,'CCR_dsince_lst_loan_pmt'
                    ,'CCR_dsince_lst_ontime_pmt'
                    ,'ClearCreditRiskScore'
                    ,'ClearBankBehaviorScoreV2'
                    ,'IsFPD'
                    ,'IsFPF'
                    ,'IsZPD'
                ]

ds_clarity = ds_loans[tradeline_cols]

# check NaNs
ds_clarity_nan = ds_clarity.isna().sum()

# select cols where total Nan values is less than 20%
tradeline_cols_selected = list(ds_clarity_nan[(ds_clarity_nan / len(ds_clarity)) < 0.2].index)
ds_clarity = ds_loans[tradeline_cols_selected]
ds_clarity = ds_clarity.dropna()

ds_clarity_zpd = ds_clarity[ds_clarity['IsZPD']==1]
ds_clarity_zpd.drop(['IsFPD', 'IsFPF', 'IsZPD'], axis=1, inplace=True)
ds_clarity_fpd = ds_clarity[ds_clarity['IsFPD']==1]
ds_clarity_fpd.drop(['IsFPD', 'IsFPF', 'IsZPD'], axis=1, inplace=True)
ds_clarity_fpf = ds_clarity[ds_clarity['IsFPF']==1]
ds_clarity_fpf.drop(['IsFPD', 'IsFPF', 'IsZPD'], axis=1, inplace=True)

create_corr(ds_clarity_zpd, 'kendall', 'Correlation Matrix - ZPD')
create_corr(ds_clarity_fpd, 'kendall', 'Correlation Matrix - FPD')
create_corr(ds_clarity_fpf, 'kendall', 'Correlation Matrix - FPF')

# Rank order

################       ZPD      ################

### ClearCreditRiskScore
zpd_tradeline_atts_ccr = [
                          'ClearCreditRiskScore',
                          'number_of_loans_past_due',
                          'days_since_last_loan_payment',
                          'number_of_bank_accounts',
                          'CCR_dsince_1st_bankacct_prv_seen',
                          'CCR_dsince_inq_prv_seen',
                          'CCR_dsince_lst_loan_opened',
                          'CCR_dsince_lst_loan_pmt'
                          ] 

for att in zpd_tradeline_atts_ccr:
    try:
        score_distribution(ds_clarity, att, 10, 'IsZPD', title='ZPD - {}'.format(str(att)))
    except:
        continue;

### ClearBankBehavior        
zpd_tradeline_atts_cbb = [
                          'ClearBankBehaviorScoreV2',
                          'number_of_accounts_at_high_risk_banks',
                          'number_of_bank_accounts',
                          'CCR_dsince_1st_bankacct_prv_seen'
                          ]         
        
for att in zpd_tradeline_atts_cbb:
    try:
        score_distribution(ds_clarity, att, 10, 'IsZPD', title='ZPD - {}'.format(str(att)))
    except:
        continue;
        

################       FPD      ################

### ClearCreditRiskScore
fpd_tradeline_atts_ccr = [
                          'ClearCreditRiskScore',
                          'number_of_loans_past_due',
                          'days_since_last_loan_payment',
                          'number_of_bank_accounts',
                          'CCR_dsince_1st_bankacct_prv_seen',
                          'CCR_dsince_inq_prv_seen',
                          'CCR_dsince_lst_loan_opened',
                          'CCR_dsince_lst_loan_pmt'
                          ] 

for att in fpd_tradeline_atts_ccr:
    try:
        score_distribution(ds_clarity, att, 10, 'IsFPD', title='FPD - {}'.format(str(att)))
    except:
        continue;

### ClearBankBehavior        
fpd_tradeline_atts_cbb = [
                          'ClearBankBehaviorScoreV2',
                          'number_of_accounts_at_high_risk_banks',
                          'number_of_bank_accounts',
                          'CCR_dsince_1st_bankacct_prv_seen'
                          ]         
        
for att in fpd_tradeline_atts_cbb:
    try:
        score_distribution(ds_clarity, att, 10, 'IsFPD', title='FPD - {}'.format(str(att)))
    except:
        continue;
        


################       FPF      ################

### ClearCreditRiskScore
fpf_tradeline_atts_ccr = [
                          'ClearCreditRiskScore',
                          'number_of_accounts_at_high_risk_banks',
                          'number_of_accounts_with_alternate_ssns',
                          'days_since_last_loan_payment',
                          'number_of_bank_accounts',
                          'CCR_dsince_1st_bankacct_prv_seen',
                          'CCR_dsince_1st_loan_opened',
                          'CCR_dsince_lst_loan_pmt'
                          ] 

for att in fpf_tradeline_atts_ccr:
    try:
        score_distribution(ds_clarity, att, 10, 'IsFPF', title='FPF - {}'.format(str(att)))
    except:
        continue;

### ClearBankBehavior        
fpf_tradeline_atts_cbb = [
                          'ClearBankBehaviorScoreV2',
                          'number_of_loans_past_due',
                          'number_of_payday_inquiries_number_since_last_inquiry',
                          'number_of_bank_accounts',
                          'CCR_dsince_1st_bankacct_prv_seen',
                          'CCR_dsince_1st_loan_opened',
                          
                          ]         
        
for att in fpf_tradeline_atts_cbb:
    try:
        score_distribution(ds_clarity, att, 10, 'IsFPF', title='FPF - {}'.format(str(att)))
    except:
        continue;
