USE [DATAWAREHOUSEMFCX]
GO

/****** Object:  View [MFCXStage].[vi_Clarityservicelog]    Script Date: 1/10/2022 9:17:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER  VIEW [MFCXStage].[vi_ClarityTradelineAttributes]
   WITH SCHEMABINDING AS
SELECT ServiceInquiryLogId
,DateRequested
,ServiceResponseDetail
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(xmlin, '\r\n', ''), '\', ''),'"{','{'), '}"','}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(xmlin, '\r\n', ''), '\', ''),'"{','{'), '}"','}'), '$.DynamicRequestObject.ssn')
ELSE 
NULL END SSN
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[0].default_history')
ELSE 
NULL END BB_Acct1_dflt_hist
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[0].validated_through_trades')
ELSE 
NULL END BB_Acct1_validated_thru_trades
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[0].default_rate_60_days_ago')
ELSE 
NULL END BB_Acct1_dflt_rate_60_days
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[0].default_rate_61_365_days_ago')
ELSE 
NULL END BB_Acct1_dflt_rate_61_365
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[0].default_rate_ratio')
ELSE 
NULL END BB_Acct1_dflt_rate_ratio
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[0].days_since_validated_trade')
ELSE 
NULL END BB_Acct1_dsnce_validated_trade
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[0].days_since_validated_trade')
ELSE 
NULL END BB_Acct1_dsnce_dflt_hist
-- Account 2
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[1].default_history')
ELSE 
NULL END BB_Acct2_dflt_hist
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[1].validated_through_trades')
ELSE 
NULL END BB_Acct2_validated_thru_trades
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[1].default_rate_60_days_ago')
ELSE 
NULL END BB_Acct2_dflt_rate_60_days
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[1].default_rate_61_365_days_ago')
ELSE 
NULL END BB_Acct2_dflt_rate_61_365
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[1].default_rate_ratio')
ELSE 
NULL END BB_Acct2_dflt_rate_ratio
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[1].days_since_validated_trade')
ELSE 
NULL END BB_Acct2_dsnce_validated_trade
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[1].days_since_validated_trade')
ELSE 
NULL END BB_Acct2_dsnce_dflt_hist
-- Account 3
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[2].default_history')
ELSE 
NULL END BB_Acct3_dflt_hist
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[2].validated_through_trades')
ELSE 
NULL END BB_Acct3_validated_thru_trades
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[2].default_rate_60_days_ago')
ELSE 
NULL END BB_Acct3_dflt_rate_60_days
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[2].default_rate_61_365_days_ago')
ELSE 
NULL END BB_Acct3_dflt_rate_61_365
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[2].default_rate_ratio')
ELSE 
NULL END BB_Acct3_dflt_rate_ratio
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[2].days_since_validated_trade')
ELSE 
NULL END BB_Acct3_dsnce_validated_trade
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[2].days_since_validated_trade')
ELSE 
NULL END BB_Acct3_dsnce_dflt_hist
-- Account 4
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[3].default_history')
ELSE 
NULL END BB_Acct4_dflt_hist
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[3].validated_through_trades')
ELSE 
NULL END BB_Acct4_validated_thru_trades
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[3].default_rate_60_days_ago')
ELSE 
NULL END BB_Acct4_dflt_rate_60_days
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[3].default_rate_61_365_days_ago')
ELSE 
NULL END BB_Acct4_dflt_rate_61_365
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[3].default_rate_ratio')
ELSE 
NULL END BB_Acct4_dflt_rate_ratio
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[3].days_since_validated_trade')
ELSE 
NULL END BB_Acct4_dsnce_validated_trade
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[3].days_since_validated_trade')
ELSE 
NULL END BB_Acct4_dsnce_dflt_hist
-- Account 5
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[4].default_history')
ELSE 
NULL END BB_Acct5_dflt_hist
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[4].validated_through_trades')
ELSE 
NULL END BB_Acct5_validated_thru_trades
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[4].default_rate_60_days_ago')
ELSE 
NULL END BB_Acct5_dflt_rate_60_days
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[4].default_rate_61_365_days_ago')
ELSE 
NULL END BB_Acct5_dflt_rate_61_365
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[4].default_rate_ratio')
ELSE 
NULL END BB_Acct5_dflt_rate_ratio
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[4].days_since_validated_trade')
ELSE 
NULL END BB_Acct5_dsnce_validated_trade
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_bank_behavior.accounts.account[4].days_since_validated_trade')
ELSE 
NULL END BB_Acct5_dsnce_dflt_hist
-- ## ClearCreditRisk ##
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.hit')
ELSE 
NULL END CCR_hit
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.too_many_tradelines')
ELSE 
NULL END CCR_too_many_tradelines
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.worst_payment_rating')
ELSE 
NULL END CCR_worst_pmt_rating
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.tradelines.tradeline.count_one_cycle_past_due')
ELSE 
NULL END CCR_count_one_cycle_past_due
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.tradelines.tradeline.count_two_cycles_past_due')
ELSE 
NULL END CCR_count_two_cycles_past_due
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.tradelines.tradeline.count_three_cycles_past_due')
ELSE 
NULL END CCR_count_three_cycles_past_due
--Suggested by Minfei
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_first_bank_account_first_seen')
ELSE 
NULL END CCR_dsince_1st_bankacct_1st_seen
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_first_bank_account_previously_seen')
ELSE 
NULL END CCR_dsince_1st_bankacct_prv_seen
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_first_loan_opened')
ELSE 
NULL END CCR_dsince_1st_loan_opened
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_first_loan_paid_off')
ELSE 
NULL END CCR_dsince_1st_loan_pd_off
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_first_ontime_payment')
ELSE 
NULL END CCR_dsince_1st_ontime_pmt
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_inquiry_first_seen')
ELSE 
NULL END CCR_dsince_inq_1st_seen
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_inquiry_previously_seen')
ELSE 
NULL END CCR_dsince_inq_prv_seen
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_last_loan_charged_off')
ELSE 
NULL END CCR_dsince_lst_loan_chrg_off
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_last_loan_in_collections')
ELSE 
NULL END CCR_dsince_lst_loan_in_colls
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_last_loan_opened')
ELSE 
NULL END CCR_dsince_lst_loan_opened
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_last_loan_paid_off')
ELSE 
NULL END CCR_dsince_lst_loan_pd_off
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_last_loan_payment')
ELSE 
NULL END CCR_dsince_lst_loan_pmt
,CASE WHEN ISJSON(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n', ''),'\', ''),'"{', '{'), '}"', '}')) = 1 THEN 
Json_value(Replace(Replace(Replace(Replace(serviceresponsedetail, '\r\n',''),'\', ''),'"{', '{'), '}"', '}'),'$.Response.xml_response.clear_credit_risk.days_since_last_ontime_payment')
ELSE 
NULL END CCR_dsince_lst_ontime_pmt
FROM    [MFCXStage].[clarityservicelog]	 
GO
